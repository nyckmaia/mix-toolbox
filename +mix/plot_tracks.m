function [h] = plot_tracks(track, reference)
%PLOT_TRACKS Plot all tracks spectrum and referece spectrum
%   Plot actual spectrum of each track object and reference spectrum
%   
%   'track' is a track object vector that contains all tracks
%   'reference' is a track object that contains the reference signal
%
%   USAGE: 
%   h = plot_tracks(track) plot all tracks spectrum without reference
%   spectrum
%
%   h = plot_tracks(track, reference) plot all tracks spectrum with reference
%   spectrum


    % Get the number of tracks:
    num_tracks = length(track);

    % Create a blank figure:
    h = figure;

    switch (nargin)
        case 1
            % Plot the 1/3 amplitude spectrum of each track signal:
            for tk = 1:num_tracks
                mix.plot(track(tk).spectrum);
                hold on
            end
            
        case 2
            % Plot 1/3 octave amplitude spectrum of the reference signal:
            h_ref = mix.plot(reference.spectrum);
            
            % Change the ploted linewidth of the reference spectrum:
            set(h_ref(1), 'linewidth', 4, 'LineStyle', '--', 'Color', 'black');
            hold on

            % Plot the 1/3 amplitude spectrum of each track signal:
            for tk = 1:num_tracks
                mix.plot(track(tk).spectrum);
            end
            
        otherwise
            error('You must set the right arguments');
    end

end