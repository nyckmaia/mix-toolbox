# Mix Toolbox

Mix Toolbox is a MATLAB is a set of functions created during the development of my master's thesis in the area of Music and Technology.

This Toolbox was made for analysis, manipulation, compression and equalization of digital audio in MATLAB.

The main purpose of this Toolbox is to provide the Automatic Mixing of musical audio files in a single final audio file, taking into account the sound balance, human acoustic perception, spectral balance and control of the amplitude variation of each audio file.

The final text of the master's dissertation referring to this Toolbox, titled "The Computational Model for Automatic Mixing for Pop Music", will be made available soon on the library website of the State University of Campinas
